package pl.hiber.rpi.server;

import net.sf.corn.cps.CPScanner;
import net.sf.corn.cps.ClassFilter;
import pl.hiber.rpi.exceptions.InitConfigException;
import pl.hiber.rpi.handlers.URLHandler;
import pl.hiber.rpi.server.handler.HandlerMethod;
import pl.hiber.rpi.viewresolvers.ViewResolver;
import winstone.Logger;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

/**
 * Created 2014 by adrians
 */
public abstract class Configuration {

	private static void log(String message) {
		Logger.logDirectMessage(Logger.INFO, Logger.DEFAULT_STREAM, message, null);
	}

	public static Configuration createConfiguration() {
		log("Creating configuration");
		List<Class<?>> classes = CPScanner.scanClasses(new ClassFilter().superClass(Configuration.class));
		Configuration configuration = new DefaultConfiguration();
		for (Class<?> cl : classes) {
			log("Merge " + cl.getSimpleName() + " to current configuration");
			Class<Configuration> configurationClass = (Class<Configuration>) cl;
			if (!configurationClass.equals(DefaultConfiguration.class)) {
				Configuration newConf = createConfiguration(configurationClass);
				configuration.mergeConfiguration(newConf);
			}
		}
		log("URL Mappings:");
		for (URLHandler handler : configuration.urlHandlers) {
			log("\t" + handler.getClass().getSimpleName());
			handler.init(configuration);
		}
		log("View resolvers:");
		for (ViewResolver viewResolver : configuration.viewResolvers) {
			log("\t" + viewResolver.getClass().getSimpleName());
			viewResolver.init(configuration);
		}
		log("Configuration created");
		return configuration;
	}

	private static Configuration createConfiguration(Class<Configuration> configurationClass) {
		try {
			return configurationClass.getDeclaredConstructor().newInstance();
		} catch (InstantiationException | IllegalAccessException |
				InvocationTargetException | NoSuchMethodException e) {
			throw new InitConfigException(e);
		}
	}

	protected String[] controllersPackages = null;
	protected ViewResolver[] viewResolvers;
	protected URLHandler[] urlHandlers;

	public void mergeConfiguration(Configuration configuration) {
		mergeControllersPackage(configuration.controllersPackages);
	}

	public String[] getControllersPackages() {
		return controllersPackages;
	}

	public ViewResolver[] getViewResolvers() {
		return viewResolvers;
	}

	public final HandlerMethod getHandlerMethod(ServletRequest request) {
		for (URLHandler handler : urlHandlers) {
			HandlerMethod handlerMethod = handler.handle(request);
			if (handlerMethod != null) {
				return handlerMethod;
			}
		}
		return null;
	}

	private void mergeControllersPackage(String[] controllersPackages) {
		String[] tmpControllersPackage = Arrays.copyOf(this.controllersPackages,
													   controllersPackages.length +
															   this.controllersPackages.length);
		int addPosition = controllersPackages.length;
		for (String pkg : controllersPackages) {
			boolean addPackage = true;
			for (String oldPkg : this.controllersPackages) {
				if (pkg.equals(oldPkg)) {
					addPackage = false;
					break;
				}
			}
			if (addPackage) {
				tmpControllersPackage[addPosition++] = pkg;
			}
		}
		this.controllersPackages = tmpControllersPackage;
	}

	public String getRootPath() {
		URL classPath = Thread.currentThread().getContextClassLoader().getResource(".");
		if (classPath == null) {
			throw new NullPointerException("classPath is null");
		}
		File rootFile = new File(classPath.getPath()).getParentFile().getParentFile();
		return rootFile.getPath();
	}

	public void resolveView(ServletRequest servletRequest,
							ServletResponse servletResponse,
							HandlerMethod handlerMethod,
							Object result,
							Model model) {
		for(ViewResolver viewResolver : viewResolvers) {
			if (viewResolver.isResolvable(servletRequest, handlerMethod, result)) {
				servletResponse.setCharacterEncoding(viewResolver.getCharacterEncoding());
				servletResponse.setContentType(viewResolver.getContentType());
				viewResolver.resolve(servletRequest, servletResponse, result, model);
				break;
			}
		}
	}
}
