package pl.hiber.rpi.server.handler;

import pl.hiber.rpi.annotations.RequestParam;
import pl.hiber.rpi.server.Model;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created 2014 by adrians
 */
public class HandlerMethodInvoker {
	public Object invoke(HandlerMethod method, HttpServletRequest request, Model model) throws ReflectiveOperationException {
		List<MethodArgument> arguments = method.getArguments();
		Object[] args = new Object[arguments.size()];
		if (args.length > 0) {
			for (int i = 0; i < args.length; i++) {
				MethodArgument argument = arguments.get(i);
				if (argument.getType() == Model.class) {
					args[i] = model;
					continue;
				}
				if (argument.getType() == HttpServletRequest.class) {
					args[i] = request;
					continue;
				}
				String[] values = request.getParameterValues(argument.getName());
				if (values != null && values.length > 0) {
					if (argument.hasAnnotation(RequestParam.class)) {
						args[i] = getObject(values, argument);
					}
				} else
					args[i] = null;
			}
		}
		return method.invoke(args);
	}

	private Object getObject(String[] values, MethodArgument argument) {
		if(argument.getType() == String.class)
			return values[0];
		if(argument.getType() == String[].class)
			return values;
		//TODO json;
		return null;
	}
}
