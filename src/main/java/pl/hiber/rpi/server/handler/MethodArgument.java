package pl.hiber.rpi.server.handler;

import pl.hiber.rpi.annotations.PathParam;
import pl.hiber.rpi.annotations.RequestParam;

import java.lang.annotation.Annotation;

/**
 * Created 2014 by adrians
 */
public class MethodArgument {
	private String name = null;
	private Class<?> type;
	private Annotation[] annotations;

	public MethodArgument(Class<?> type, Annotation[] annotations) {
		this.type = type;
		this.annotations = annotations;
		for(Annotation annotation : annotations) {
			if(annotation instanceof RequestParam)
				name = ((RequestParam) annotation).value();
			else if(annotation instanceof PathParam)
				name = ((PathParam) annotation).value();
			if (name != null)
				break;
		}
	}

	public String getName() {
		return name;
	}

	public Class<?> getType() {
		return type;
	}

	public Annotation getAnnotation(Class<? extends Annotation> type) {
		for(Annotation annotation : annotations) {
			if (annotation.annotationType() == type)
				return annotation;
		}
		return null;
	}

	public boolean hasAnnotation(Class<? extends Annotation> type) {
		return getAnnotation(type) != null;
	}
}
