package pl.hiber.rpi.server.handler;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created 2014 by adrians
 */
public class HandlerMethod {
	private Class<?> className;
	private Method method;
	private String url;
	private List<MethodArgument> arguments;

	private Object controller;

	public HandlerMethod(Class<?> className, Method method, String value) {
		this.className = className;
		this.method = method;
		this.url = value;
		arguments = new ArrayList<MethodArgument>(1);
	}

	public void addArgument(Class<?> type, Annotation[] annotations) {
		arguments.add(new MethodArgument(type, annotations));
	}

	public Method getMethod() {
		return method;
	}

	public Class<?> getClassName() {
		return className;
	}

	protected Object invoke(Object[] args)
			throws NoSuchMethodException, IllegalAccessException,
			InvocationTargetException, InstantiationException {
		if (controller == null)
			controller = className.getConstructor(new Class<?>[0]).newInstance(new Object[0]);
		return method.invoke(controller, args);
	}

	public void setController(Object controller) {
		this.controller = controller;
	}

	public Object getController() {
		return controller;
	}

	public List<MethodArgument> getArguments() {
		return arguments;
	}

	public String getUrl() {
		return url;
	}
}
