package pl.hiber.rpi.server;

import pl.hiber.rpi.handlers.StaticHandler;
import pl.hiber.rpi.handlers.URLHandler;
import pl.hiber.rpi.viewresolvers.HtmlResolver;
import pl.hiber.rpi.viewresolvers.JsonResolver;
import pl.hiber.rpi.viewresolvers.ViewResolver;

/**
 * Created 2014 by adrians
 */
public class DefaultConfiguration extends Configuration {
	public DefaultConfiguration() {
		this.controllersPackages = new String[] { "pl.hiber.rpi.controllers" };
		this.urlHandlers = new URLHandler[] { new StaticHandler() };
		this.viewResolvers = new ViewResolver[] { new HtmlResolver(), new JsonResolver() };
	}
}
