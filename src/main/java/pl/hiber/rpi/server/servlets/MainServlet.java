package pl.hiber.rpi.server.servlets;

import pl.hiber.rpi.exceptions.ViewResolveException;
import pl.hiber.rpi.server.Configuration;
import pl.hiber.rpi.server.Model;
import pl.hiber.rpi.server.handler.HandlerMethod;
import pl.hiber.rpi.server.handler.HandlerMethodInvoker;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.WeakHashMap;

/**
 * Created 2014 by adrians
 */
public class MainServlet extends GenericServlet {
	private Configuration configuration;
	private HandlerMethodInvoker handlerMethodInvoker;
	private WeakHashMap<Class<?>, Object> controllersMap = new WeakHashMap<>(3, 0.7f);

	@Override public void destroy() {
		log("Destroy");
	}

	@Override public void init(ServletConfig servletConfig) throws ServletException {
		configuration = Configuration.createConfiguration();
		handlerMethodInvoker = new HandlerMethodInvoker();
	}

	@Override
	public void service(ServletRequest servletRequest, ServletResponse servletResponse)
			throws ServletException {
		long stopTime, startTime = System.currentTimeMillis();
		HandlerMethod handlerMethod = configuration.getHandlerMethod(servletRequest);
		if (handlerMethod != null) {
			Object result;
			try {
				Model model = new Model();
				Object controller = controllersMap.get(handlerMethod.getClassName());
				handlerMethod.setController(controller);
				result = handlerMethodInvoker.invoke(handlerMethod, (HttpServletRequest) servletRequest,
													 model);
				controllersMap.put(handlerMethod.getClassName(), handlerMethod.getController());
				configuration.resolveView(servletRequest, servletResponse,
										  handlerMethod, result, model);
			} catch (ReflectiveOperationException e) {
				throw new ViewResolveException(e);
			} finally {
				stopTime = System.currentTimeMillis();
				System.out.println("Processing time: " + (stopTime - startTime));
			}
		}
		else {
			((HttpServletResponse)servletResponse).setStatus(404);
		}
	}
}
