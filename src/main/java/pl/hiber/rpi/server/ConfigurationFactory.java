package pl.hiber.rpi.server;

import pl.hiber.rpi.handlers.URLHandler;
import pl.hiber.rpi.viewresolvers.ViewResolver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created 2014 by adrians
 */
public class ConfigurationFactory {
	private List<String> controllerPackages = new ArrayList<>();
	private List<URLHandler> urlHandlers = new ArrayList<>();
	private List<ViewResolver> viewResolvers = new ArrayList<>();

	public ConfigurationFactory addControllerPackage(String controllerPackage) {
		controllerPackages.add(controllerPackage);
		return this;
	}

	public ConfigurationFactory addUrlHandler(URLHandler urlHandler) {
		urlHandlers.add(urlHandler);
		return this;
	}

	public ConfigurationFactory addViewResolver(ViewResolver viewResolver) {
		viewResolvers.add(viewResolver);
		return this;
	}

	public Configuration build() {
		Configuration configuration = new DefaultConfiguration();
		configuration.controllersPackages = controllerPackages.toArray(new String[controllerPackages.size()]);
		configuration.viewResolvers = viewResolvers.toArray(new ViewResolver[viewResolvers.size()]);
		configuration.urlHandlers = urlHandlers.toArray(new URLHandler[urlHandlers.size()]);
		return configuration;
	}
}
