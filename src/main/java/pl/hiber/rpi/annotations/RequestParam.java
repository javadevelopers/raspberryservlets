package pl.hiber.rpi.annotations;

import java.lang.annotation.*;

/**
 * Created 2014 by adrians
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface RequestParam {
	String value() default "";
}
