package pl.hiber.rpi.annotations;

import java.lang.annotation.*;

/**
 * Created 2014 by adrians
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Request {
	String value() default "/";
}
