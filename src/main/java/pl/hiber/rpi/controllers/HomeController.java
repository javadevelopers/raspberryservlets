package pl.hiber.rpi.controllers;

import pl.hiber.rpi.annotations.Request;
import pl.hiber.rpi.server.Model;
import pl.hiber.rpi.utils.SysInfoUtils;

/**
 * Created 2014 by adrians
 */
public class HomeController {

	@Request("/")
	public String testIndex(Model model) {
		model.put("test", new String[] {"ala", "ma", SysInfoUtils.readTemp()});
		return "test";
	}
}
