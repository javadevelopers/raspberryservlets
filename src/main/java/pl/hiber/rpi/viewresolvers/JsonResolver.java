package pl.hiber.rpi.viewresolvers;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.map.ObjectMapper;
import pl.hiber.rpi.exceptions.ViewResolveException;
import pl.hiber.rpi.server.Model;
import pl.hiber.rpi.server.handler.HandlerMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.util.regex.Pattern;

/**
 * Created 2014 by adrians
 */
public class JsonResolver extends ViewResolver {
	private JsonFactory jsonFactory = new JsonFactory();
	private ObjectCodec objectCodec = new ObjectMapper();

	private static Pattern varPattern = Pattern.compile("\\$\\{(.*?)\\}");

	@Override public String getCharacterEncoding() {
		return "UTF-8";
	}

	@Override public String getContentType() {
		return "application/json";
	}

	@Override
	public boolean isResolvable(ServletRequest servletRequest,
										  HandlerMethod handlerMethod,
										  Object methodResult) {
		return methodResult != null && !(methodResult instanceof String);
	}

	@Override
	public void resolve(ServletRequest servletRequest, ServletResponse servletResponse,
						Object methodResult, Model model) {
		try {
			OutputStream outputStream = servletResponse.getOutputStream();
			JsonGenerator jsonGenerator = jsonFactory.createJsonGenerator(outputStream);
			jsonGenerator.setCodec(objectCodec);
			jsonGenerator.writeObject(methodResult);
			jsonGenerator.flush();
			jsonGenerator.close();
		} catch (IOException e) {
			throw new ViewResolveException("jsonResolve", e);
		}
	}
}
