package pl.hiber.rpi.viewresolvers;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.map.ObjectMapper;
import pl.hiber.rpi.exceptions.ViewNotFoundException;
import pl.hiber.rpi.exceptions.ViewResolveException;
import pl.hiber.rpi.server.Model;
import pl.hiber.rpi.server.handler.HandlerMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created 2014 by adrians
 */
public class HtmlResolver extends ViewResolver {

	private JsonFactory jsonFactory = new JsonFactory();
	private ObjectCodec objectCodec = new ObjectMapper();

	private static Pattern varPattern = Pattern.compile("\\$\\{(.*?)\\}");

	@Override public String getCharacterEncoding() {
		return "UTF-8";
	}

	@Override public String getContentType() {
		return "text/html";
	}

	@Override
	public boolean isResolvable(ServletRequest servletRequest,
										  HandlerMethod handlerMethod,
										  Object methodResult) {
		return methodResult != null && methodResult instanceof String;
	}

	@Override
	public void resolve(ServletRequest servletRequest, ServletResponse servletResponse,
						Object methodResult, Model model) {
		try {
			PrintWriter printWriter = servletResponse.getWriter();

			File file = new File(configuration.getRootPath(), "views/" + methodResult + ".html");
			if(!file.exists()) {
				throw new ViewNotFoundException((String) methodResult);
			}
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line;
			while((line = reader.readLine()) != null ) {
				StringBuffer sb = new StringBuffer();
				Matcher matcher = varPattern.matcher(line);
				while(matcher.find()) {
					String varName = matcher.group(1);
					Object result = model.get(varName);
					ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
					printObjectToOutputStream(result, outputStream);
					matcher.appendReplacement(sb, new String(outputStream.toByteArray()));
				}
				matcher.appendTail(sb);
				printWriter.println(sb);
			}
			reader.close();
			printWriter.flush();
			printWriter.close();
		} catch (IOException e) {
			throw new ViewResolveException((String) methodResult, e);
		}
	}

	/** Zapisuje objekt do outputStream */
	private void printObjectToOutputStream(Object result, OutputStream outputStream) throws IOException {
		JsonGenerator jsonGenerator = jsonFactory.createJsonGenerator(outputStream);
		jsonGenerator.setCodec(objectCodec);
		jsonGenerator.writeObject(result);
		jsonGenerator.flush();
		jsonGenerator.close();
	}
}
