package pl.hiber.rpi.viewresolvers;

import pl.hiber.rpi.exceptions.ViewResolveException;
import pl.hiber.rpi.server.Configuration;
import pl.hiber.rpi.server.Model;
import pl.hiber.rpi.server.handler.HandlerMethod;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Created 2014 by adrians
 */
public abstract class ViewResolver {
	protected Configuration configuration;

	public abstract String getCharacterEncoding();
	public abstract String getContentType();
	public abstract boolean isResolvable(ServletRequest servletRequest,
										 HandlerMethod handlerMethod, Object methodResult);
	public abstract void resolve(ServletRequest servletRequest, ServletResponse servletResponse,
								 Object methodResult, Model model)
			throws ViewResolveException;

	public void init(Configuration configuration) {
		this.configuration = configuration;
	}
}
