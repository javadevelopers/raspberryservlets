package pl.hiber.rpi.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created 2014 by adrians
 */
public class SysInfoUtils {
	private static File THERMAL_PATH = new File("/sys/class/thermal/thermal_zone0/temp");
	public SysInfoUtils() {
	}

	public static short readTemp() {
		try {
			String temp = FileUtils.readFileToString(THERMAL_PATH);
			if (temp != null) {
				return Short.parseShort(temp.trim());
			}
		} catch (IOException e) {
			//TODO logger;
		}
		return -1;
	}
}
