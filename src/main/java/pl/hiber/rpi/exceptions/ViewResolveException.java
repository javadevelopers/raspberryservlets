package pl.hiber.rpi.exceptions;

/**
 * Created 2014 by adrians
 */
public class ViewResolveException extends RuntimeException {
	public ViewResolveException(String message) {
		super(message);
	}

	public ViewResolveException(String message, Throwable cause) {
		super(message, cause);
	}

	public ViewResolveException(Throwable cause) {
		super(cause);
	}

	public ViewResolveException(String message, Throwable cause, boolean enableSuppression,
								boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
