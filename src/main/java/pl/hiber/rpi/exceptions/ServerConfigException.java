package pl.hiber.rpi.exceptions;

/**
 * Created 2014 by adrians
 */
public class ServerConfigException extends RuntimeException {
	public ServerConfigException(String message) {
		super(message);
	}

	public ServerConfigException(String message, Throwable cause) {
		super(message, cause);
	}

	public ServerConfigException(Throwable cause) {
		super(cause);
	}

	public ServerConfigException(String message, Throwable cause, boolean enableSuppression,
								 boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
