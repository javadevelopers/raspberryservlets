package pl.hiber.rpi.exceptions;

/**
 * Created 2014 by adrians
 */
public class InitConfigException extends RuntimeException {
	public InitConfigException(Throwable cause) {
		super(cause);
	}
}
