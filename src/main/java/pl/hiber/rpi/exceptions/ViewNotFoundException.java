package pl.hiber.rpi.exceptions;

/**
 * Created 2014 by adrians
 */
public class ViewNotFoundException extends ViewResolveException {
	public ViewNotFoundException(String message) {
		super(message);
	}

	public ViewNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public ViewNotFoundException(Throwable cause) {
		super(cause);
	}

	public ViewNotFoundException(String message, Throwable cause, boolean enableSuppression,
								 boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
