package pl.hiber.rpi.handlers;

import net.sf.corn.cps.CPScanner;
import net.sf.corn.cps.ClassFilter;
import pl.hiber.rpi.annotations.Request;
import pl.hiber.rpi.server.Configuration;
import pl.hiber.rpi.server.handler.HandlerMethod;
import winstone.WinstoneRequest;

import javax.servlet.ServletRequest;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created 2014 by adrians
 */
public class StaticHandler extends URLHandler {
	private Map<String, HandlerMethod> requestsMap;
	private Configuration configuration;

	@Override public void init(Configuration configuration) {
		requestsMap = new HashMap<>(10,1);
		this.configuration = configuration;

		for (String pkg : configuration.getControllersPackages())
			mapControllers(pkg);
	}

	@Override
	public HandlerMethod handle(ServletRequest httpServletRequest) {
		return requestsMap.get(((WinstoneRequest) httpServletRequest).getRequestURI());
	}

	private void mapControllers(String pkg) {
		List<Class<?>> classes = CPScanner.scanClasses(new ClassFilter().packageName(pkg));
		if(classes == null || classes.size() == 0) {
			return;
		}
		for(Class<?> cClass : classes) {
			logMapping(cClass.getSimpleName());
			Method[] methods = cClass.getDeclaredMethods();
			for(Method method : methods) {
				Request request = method.getAnnotation(Request.class);
				HandlerMethod handlerMethod = new HandlerMethod(cClass, method, request.value());
				Annotation[][] annotations = method.getParameterAnnotations();
				Class<?>[] parameters = method.getParameterTypes();
				for(int i = 0; i < annotations.length; i++) {
					handlerMethod.addArgument(parameters[i], annotations[i]);
				}
				requestsMap.put(request.value(), handlerMethod);
				logMapping(request.value(), method);
			}
		}
	}
}
