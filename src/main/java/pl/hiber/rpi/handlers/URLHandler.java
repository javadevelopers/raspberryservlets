package pl.hiber.rpi.handlers;

import pl.hiber.rpi.server.Configuration;
import pl.hiber.rpi.server.handler.HandlerMethod;
import winstone.Logger;

import javax.servlet.ServletRequest;
import java.lang.reflect.Method;

/**
 * Created 2014 by adrians
 */
public abstract class URLHandler {
	public abstract void init(Configuration configuration);

	public abstract HandlerMethod handle(ServletRequest httpServletRequest);

	protected void logMapping(String mapTo, Method method) {
		Logger.logDirectMessage(Logger.INFO, Logger.DEFAULT_STREAM,
								"\t\t" + mapTo + " => " + method.getName(), null);
	}

	protected void logMapping(String mapTo) {
		Logger.logDirectMessage(Logger.INFO, Logger.DEFAULT_STREAM,
								"\t\t" + mapTo, null);
	}
}
